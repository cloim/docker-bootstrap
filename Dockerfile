FROM cloim/alpine
MAINTAINER cloim <cloimism@gmail.com>

VOLUME /etc/certs
VOLUME /var/log
VOLUME /etc/conf.d/nginx

RUN apk update; apk add --no-cache \
    nginx \
    nodejs \
    && rm -rf /var/www/localhost \
    && mkdir -p /etc/templates

COPY ./manifest/ /
RUN chmod u+rwx /entrypoint.sh

EXPOSE 80 443

ENTRYPOINT ["/bin/bash", "/entrypoint.sh"]
