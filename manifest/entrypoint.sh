#!/bin/bash
set -e

function copyTemplates() {
  echo "# Copy Templates Start"

  find /etc/templates/nginx/ -type f -iname "*.conf" -print0 | while IFS= read -r -d '' filename
  do
    if [ -f "/etc/conf.d/nginx/${filename##*/}" ]; then
      echo "    ${filename##*/} already exists"
    else
      echo "    Copy ${filename##*/}"
      cp /etc/templates/nginx/${filename##*/} /etc/conf.d/nginx/
    fi
  done

  if [ -f "/var/www/index.html" ]; then
    echo "    index.html already exists"
  else
    echo "    Copy index.html"
    cp /etc/templates/www/index.html /var/www/
  fi

  echo "# Copy Templates Done"
  echo " "
}

function applyVariables() {
  echo "# Apply Variables Start"

  if [ "$USE_SSL" == "yes" ]; then
    echo "    SSL enabled"
  else
    echo "    SSL disabled"
    rm -f /etc/conf.d/nginx/default-ssl.conf
  fi

  echo "# Apply Variables Done"
  echo " "
}

function generateThings() {
  echo "# Generate Things Start"

  cd /var/www
  npm install bootstrap &> /dev/null
  npm install jquery &> /dev/null
  mkdir -p /var/log/nginx

  echo "# Generate Things Done"
  echo " "
}

copyTemplates
applyVariables
generateThings

echo "# BootStrap Start"
echo " "
exec /usr/sbin/nginx
