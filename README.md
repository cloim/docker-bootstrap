# Customized nginx Server with BootStrap
Based on ([cloim/alpine](https://hub.docker.com/r/cloim/alpine/))

Default configurations will be placed in /etc/conf.d/nginx (default.conf, default-ssl.conf)
Default html will be placed in /var/www (index.html)

## Dockerfile links
* [latest](https://gitlab.com/cloim/docker-bootstrap)

# Examples
## Simple way
```
docker run -d -t \
           --name bootstrap \
           -p 80:80 \
           -p 443:443 \
           cloim/bootstrap
```

## Availables
If you want to use ssl, Just set 'USE_SSL' ENV to 'yes' (Require certification files, fullchain.pem & privkey.pem in /etc/certs).
```
docker run -d -t \
           --name bootstrap \
           -p 80:80 \
           -p 443:443 \
           -e 'USE_SSL=yes' \
           -v ~/Docker/Data/bootstrap/www:/var/www \
           -v ~/Docker/Data/bootstrap/log:/var/log \
           -v ~/Docker/Data/certs:/etc/certs \
           -v ~/Docker/Config/bootstrap:/etc/conf.d/nginx \
           cloim/bootstrap
```
